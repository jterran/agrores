﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Agrores
{
    /// <summary>
    /// Lógica de interacción para MenuPrincipal.xaml
    /// </summary>
    public partial class MenuPrincipal : Window
    {
        public MenuPrincipal()
        {
            InitializeComponent();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void btnIngreso_Click(object sender, RoutedEventArgs e)
        {
           
            Ingreso miMenu = new Ingreso();
            pantalla.Tag = miMenu;
            miMenu.Show();
        }

        private void btnEstancia_Click(object sender, RoutedEventArgs e)
        {
            Estancia pantalla = new Estancia();
            pantalla.Owner = this;
            pantalla.ShowDialog();
        }

        private void btnSalida_Click(object sender, RoutedEventArgs e)
        {
            Salida pantalla = new Salida();
            pantalla.Owner = this;
            pantalla.ShowDialog();
        }

        private void btnEnObservacion_Click(object sender, RoutedEventArgs e)
        {
            WPFEnobservacion pantalla = new WPFEnobservacion();
            pantalla.Owner = this;
            pantalla.ShowDialog();
        }

        private void btnConsultas_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
