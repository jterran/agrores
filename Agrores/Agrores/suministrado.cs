//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Agrores
{
    using System;
    using System.Collections.Generic;
    
    public partial class suministrado
    {
        public int id { get; set; }
        public int animal_numero_registro { get; set; }
        public int farmaco_id_farmaco { get; set; }
    
        public virtual animal animal { get; set; }
        public virtual farmaco farmaco { get; set; }
    }
}
